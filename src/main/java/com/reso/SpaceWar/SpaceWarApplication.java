package com.reso.SpaceWar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaceWarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpaceWarApplication.class, args);
	}

}
